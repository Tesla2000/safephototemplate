package com.zpo.project.safephoto.domain.interfaces;

import com.zpo.project.safephoto.domain.Customer;
import com.zpo.project.safephoto.domain.Photo;

import java.io.IOException;

public interface ICustomerRepository {
    Customer getOneByToken(String token) throws IOException;
    void savePhoto(Customer customer, Photo photo);
}
