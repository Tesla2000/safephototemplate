package com.zpo.project.safephoto.domain.interfaces;

import com.zpo.project.safephoto.domain.projections.PhotoProjection;

public interface IPhotoProjector {
    PhotoProjection getById(long id, String token);
    PhotoProjection getManyByCustomerToken(String token);
}
