package com.zpo.project.safephoto.domain;

import com.zpo.project.safephoto.domain.snapshots.MetadataSnapshot;
import java.time.LocalDateTime;

public class Metadata {
    private String title;
    private String location;
    private LocalDateTime date;
    private String description;
    private String format;

    public Metadata(String title, String location, LocalDateTime date, String description, String format) {
        this.title = title;
        this.location = location;
        this.date = date;
        this.description = description;
        this.format = format;
    }

    public static Metadata create(String title, String location, LocalDateTime date, String description, String format){
        // walidacja
        return new Metadata(title, location, date, description, format);
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getFormat() {
        return format;
    }

    public static Metadata restoreFromSnapshot(MetadataSnapshot snapshot){
        return new Metadata(snapshot.getTitle(), snapshot.getLocation(), snapshot.getDate(), snapshot.getDescription(), snapshot.getFormat());
    }
}
