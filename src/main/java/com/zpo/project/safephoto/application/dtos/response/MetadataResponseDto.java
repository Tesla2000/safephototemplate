package com.zpo.project.safephoto.application.dtos.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MetadataResponseDto {
    private String title;
    private String location;
    private String date;
    private String description;
    private String format;

    public MetadataResponseDto(String title, String location, LocalDateTime date, String description, String format) {
        this.title = title;
        this.location = location;
        this.date = date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        this.description = description;
        this.format = format;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDate(LocalDateTime date) {
        this.date = date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getFormat() {
        return format;
    }
}
