package com.zpo.project.safephoto.infrastructure.services;

import com.zpo.project.safephoto.infrastructure.interfaces.ICustomerTokenDecoder;
import com.zpo.project.safephoto.infrastructure.models.CustomerModel;
import org.springframework.stereotype.Service;

@Service
public class CustomerTokenDecoder implements ICustomerTokenDecoder {
    @Override
    public CustomerModel getCustomerFromToken(String token) {
        var customer = new CustomerModel();
        customer.setId(1);
        customer.setPassword("password");
        customer.setSalt("salt");
        return customer;
    }
}
