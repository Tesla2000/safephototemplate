package com.zpo.project.safephoto.infrastructure;

import com.zpo.project.safephoto.domain.Customer;
import com.zpo.project.safephoto.domain.Photo;
import com.zpo.project.safephoto.domain.interfaces.ICustomerRepository;
import com.zpo.project.safephoto.domain.snapshots.CustomerSnapshot;
import com.zpo.project.safephoto.infrastructure.interfaces.ICustomerService;
import com.zpo.project.safephoto.infrastructure.interfaces.ICustomerTokenDecoder;
import com.zpo.project.safephoto.infrastructure.interfaces.ICypherService;
import com.zpo.project.safephoto.infrastructure.interfaces.IFileService;
import com.zpo.project.safephoto.infrastructure.models.CustomerModel;
import com.zpo.project.safephoto.infrastructure.models.MetadataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.crypto.SecretKey;
import java.io.IOException;

/***
 * Służy do pobierania agregatu/encji w celu zmiany jego stanu (dodawanie, edycja itd.)
 */
@Repository
public class CustomerRepository implements ICustomerRepository {
    private ICypherService cypherService;
    private ICustomerTokenDecoder customerTokenDecoder;
    private ICustomerService customerService;
    private IFileService fileService;

    @Autowired
    public CustomerRepository(ICypherService cypherService, ICustomerTokenDecoder customerTokenDecoder, ICustomerService customerService, IFileService fileService) {
        this.cypherService = cypherService;
        this.customerTokenDecoder = customerTokenDecoder;
        this.customerService = customerService;
        this.fileService = fileService;
    }

    @Override
    public Customer getOneByToken(String token) throws IOException {
        var customerModel = composeCustomer(token);
        var customerKey = getCustomerKey(customerModel);

        var fileList = fileService.listFilesInDirectory("photos/" + customerModel.getId());
        var metadataList = fileList.stream().filter(name -> name.contains(".metadata")).toList();
        var photoList = fileList.stream().filter(name -> !name.contains(".metadata")).toList();

        // wczytujemy z pliku zdjęcia i odpowiadające im meta
        // parsujemy string na obiekty PhotoModel i MetadataModel
        // uzupełniamy CustomerModel

        // dekodujemy zdjęcia i metadata
        // parsujemy na snapshot
        // uzupełniamy CustomerSnapshot o odszyfrowane dane

        return Customer.restoreFromSnapshot(new CustomerSnapshot(null, customerKey, customerModel.getId())); // tutaj ważne, żeby new Customer zamienić na zmapowany na snapshot customerFromToken
    }

    private SecretKey getCustomerKey(CustomerModel customer) {
        return cypherService.keyFromString(
                new String(cypherService.decrypt(
                        customer.getEncryptedKey(), customer.getPassword(), customer.getSalt(), cypherService.ivFromString(customer.getBase64Iv())
                ))
        );
    }

    @Override
    public void savePhoto(Customer customer, Photo photo) {
        var metadataString = customerService.serializeToString(photo.getMetadata());
        var metadataIv = cypherService.generateIv(16);
        var encryptedMetadata = cypherService.encrypt(metadataString.getBytes(), customer.getKey(), metadataIv);
        var metadataModel = new MetadataModel(encryptedMetadata, cypherService.ivToString(metadataIv));
        var metadataModelString = customerService.serializeToString(metadataModel);
        // analogicznie dla zjecia
        // tutaj będzie zapis do pliku
    }

    private CustomerModel composeCustomer(String token) throws IOException {
        var customer = customerTokenDecoder.getCustomerFromToken(token);
        var customerString = fileService.loadFromFile("customer/" + customer.getId());
        var customerFromFile = customerService.parseFromString(customerString);
        customer.setEncryptedKey(customerFromFile.getEncryptedKey());
        customer.setBase64Iv(customerFromFile.getBase64Iv());
        customer.setSalt(customerFromFile.getSalt());
        return customer;
    }
}
