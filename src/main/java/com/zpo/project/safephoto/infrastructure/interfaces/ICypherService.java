package com.zpo.project.safephoto.infrastructure.interfaces;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public interface ICypherService {
    String encrypt(byte[] bytesToEncode, SecretKey key, IvParameterSpec iv);
    byte[] decrypt(String stringToDecode, SecretKey key, IvParameterSpec iv);
    String encrypt(byte[] bytesToEncode, String password, String salt, IvParameterSpec iv);
    byte[] decrypt(String stringToDecode, String password, String salt, IvParameterSpec iv);
    IvParameterSpec generateIv(int length);

    String ivToString(IvParameterSpec iv);
    IvParameterSpec ivFromString(String iv);
    String keyToString(SecretKey secretKey);
    SecretKey keyFromString(String encodedKey);
}
