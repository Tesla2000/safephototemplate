package com.zpo.project.safephoto.infrastructure.models;

public class PhotoModel {
    private String photo;
    private MetadataModel metadata;
    private long id;
    private String base64Iv;

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setMetadata(MetadataModel metadata) {
        this.metadata = metadata;
    }

    public String getPhoto() {
        return photo;
    }

    public MetadataModel getMetadata() {
        return metadata;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getBase64Iv() {
        return base64Iv;
    }

    public void setBase64Iv(String base64Iv) {
        this.base64Iv = base64Iv;
    }

    public PhotoModel(String photo, MetadataModel metadata, long id, String base64Iv) {
        this.photo = photo;
        this.metadata = metadata;
        this.id = id;
        this.base64Iv = base64Iv;
    }
}
