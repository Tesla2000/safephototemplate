package com.zpo.project.safephoto.infrastructure.services;

import com.zpo.project.safephoto.infrastructure.interfaces.ICypherService;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

/***
 * Dodanie do infrastruktury serwisu pozwala na odseparowanie operacji na danych do oddzielnych klas
 * Nie zapisują i nie odczytują danych z pamięci, ale modyfikują te dane jeśli baza nie potrafi (np. szyfrowanie)
 */
@Component
public class CypherService implements ICypherService {
    @Override
    public String encrypt(byte[] bytesToEncode, SecretKey key, IvParameterSpec iv) {
        // AES encode
        // base64 encode
        return new String(bytesToEncode);
    }

    @Override
    public byte[] decrypt(String stringToDecode, SecretKey key, IvParameterSpec iv) {
        // base64 decode
        // AES decode
        return stringToDecode.getBytes();
    }

    @Override
    public String encrypt(byte[] bytesToEncode, String password, String salt, IvParameterSpec iv) {
        // get key from password
        // encode(bytesToEncode, keyFromPassword, iv);
        return null;
    }

    @Override
    public byte[] decrypt(String stringToDecode, String password, String salt, IvParameterSpec iv) {
        return new byte[0];
    }

    @Override
    public IvParameterSpec generateIv(int length) {
        return null;
    }

    @Override
    public String ivToString(IvParameterSpec iv){
        return Base64.getEncoder().encodeToString(iv.getIV());
    }

    @Override
    public IvParameterSpec ivFromString(String iv){
        return new IvParameterSpec(Base64.getDecoder().decode(iv));
    }

    @Override
    public String keyToString(SecretKey secretKey){
        byte[] rawData = secretKey.getEncoded();
        return Base64.getEncoder().encodeToString(rawData);
    }

    @Override
    public SecretKey keyFromString(String encodedKey){
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }
}
